import React, { Component } from 'react'
import preload from '../data.json'
import ShowCard from './ShowCard'

export default class Search extends Component {

  state = {
    searchTerm: ''
  }

  onUserSearch = (event) => {
    this.setState({ searchTerm: event.target.value })
  }

  render() {
    return (
      <div className="search">
        <header>
          <h1>svideo</h1>
          <input
            onChange={this.onUserSearch}
            value={this.state.searchTerm}
            type="text" placeholder="Search..."
          />
        </header>
        <div>
          {preload.shows
            .filter(show => {
              return `${show.title} ${show.description}`
                .toLowerCase()
                .indexOf(this.state.searchTerm.toLowerCase()) >= 0;
            })
            .map(show => <ShowCard key={show.imdbID} show={show}/>)}
        </div>
      </div>
    )
  }

}

